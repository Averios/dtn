#!/usr/bin/python
import time
from socket import *
from threading import Thread, Lock
#import netifaces
import string
import random
import pickle
import hashlib
import math
import signal
import sys
import os

def sigint_handler(signum, frame):
	sys.exit()

packet_list = {}
#response_list = {}
connection_list = []
mutex = Lock()
#signal.signal(signal.SIGINT, sigint_handler)
myName = ''
packet_id = 0
posX = 0
posY = 0
max_distance = 100

class Message:
	source = ''
	packet_id = 0
	lifetime = 30
	ttl = 10
	read = False
	destination = ''
	response = False
	posX = 0
	posY = 0

class Packet(Message):
	message = ''

class Response(Message):
	received_id = 0
	response = True

def PacketLifetime(UDPOut, addr):
	start = time.time()
	global packet_id
	while True:
		elapsed = time.time() - start
		for key, packet in packet_list.items():
			packet.lifetime = packet.lifetime - elapsed
			sender_distance = math.sqrt(math.pow((packet.posX - posX), 2) + math.pow((packet.posY - posY), 2))
			if packet.destination == myName and not packet.read:
				#print 'You got a package from ' + (packet.source)
				print 'Some paket for you'
				print packet.__class__.__name__
				if packet.__class__.__name__ == 'Packet':
					print packet.source + ':'
					print packet.message

					#Sent ACK back to sender
					response = Response()
					response.source = myName
					response.packet_id = packet_id
					hash_key = hashlib.sha224(myName + '.' + str(packet_id)).hexdigest()
					packet_id += 1
					response.destination = packet.source
					response.received_id = packet.packet_id
					response.posX = posX
					response.posY = posY
					packet_list.update({hash_key : response})
					SendMessage(UDPOut, addr, response)
				else:
					print 'Message number ' + str(packet.received_id) + ' received'

				#Save the message to a file
				packet.read = True

			elif packet.lifetime <= 0 or packet.ttl < 0 or sender_distance > max_distance:
				if sender_distance > max_distance:
					print key + "'s source is too far"
				del packet_list[key]
			else:
				SendMessage(UDPOut, addr, packet)
		time.sleep(1)
		start = time.time()

def ReceiveMessage(UDPSock, addr):
	while True:
		data, addr = UDPSock.recvfrom(1024)
		#ubah bagian sini
		#taruh object ke packet_list
		#Key-nya pakai hash dari alamat pengirim dan id packet
		serialized = pickle.loads(data)
		hash_key = hashlib.sha224(serialized.source + '.' + str(serialized.packet_id)).hexdigest()
		if hash_key not in packet_list:
			#print hash_key
			#print serialized.source + ':'
			#print serialized.message
			serialized.ttl = serialized.ttl - 1
			packet_list.update({hash_key : serialized})

def SendMessage(UDPSock, addr, packet):
	#Serialize object
	#kirim object lewat Socket
	serialized = pickle.dumps(packet)
	UDPSock.sendto(serialized, addr)

def id_generator(size = 10, chars = string.ascii_letters + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

if __name__ == '__main__':
	#myIP = netifaces.ifaddresses('wlan0')[netifaces.AF_INET][0]['addr']
	#Set some parameters
	print 'Set your name'
	myName = raw_input('Your name: ')
	print 'Set your location (X, Y):'
	posX = input('X position = ')
	posY = input('Y position = ')
	##################

	print 'X, Y = ' + str(posX) + ', ' + str(posY)

	#Set receiver and broadcast
	addr = ('', 5061)
	global packet_id
	packet_id = 1
	UDPSock = socket(AF_INET, SOCK_DGRAM)
	UDPSock.bind(addr)
	receiver = Thread(target = ReceiveMessage, args = (UDPSock, addr))
	receiver.start()
	addrOut = ('255.255.255.255', 5061)
	UDPOut = socket(AF_INET, SOCK_DGRAM)
	UDPOut.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	UDPOut.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
	#Packet lifetime manager
	lifetime = Thread(target = PacketLifetime, args=(UDPOut, addrOut))
	lifetime.start()
	#####################################
	try:
		while True:
			#Prompt
			print 'Select your choice'
			print '1. Send message'
			print '2. Change location'
			print '0. Exit program'
			choice = input('Choice: ')
			if choice == 1:
				destination = raw_input('Destination: ')
				message = raw_input('Message: ')
				#print destination
				if(len(destination) > 0):
					#myIP = netifaces.ifaddresses('wlan0')[netifaces.AF_INET][0]['addr']
					myID = packet_id
					print 'Message id: ' + str(myID)

					packet_id = packet_id + 1
					hash_key = hashlib.sha224(myName + '.' + str(packet_id)).hexdigest()

					NewPacket = Packet()
					NewPacket.packet_id = myID
					NewPacket.source = myName
					NewPacket.destination = destination
					NewPacket.message = message
					packet_list.update({hash_key : NewPacket})
					SendMessage(UDPOut, addrOut, NewPacket)
			elif choice == 2:
				posX = raw_input('Input X value: ')
				posY = raw_input('Input Y value: ')
			else:
				print 'Exiting the program'
				break
				try:
					sys.exit()
				except SystemExit:
					os._exit(0)

	except KeyboardInterrupt:
		try:
			sys.exit()
		except SystemExit:
			os._exit(0)
